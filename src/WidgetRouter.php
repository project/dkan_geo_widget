<?php

namespace Drupal\dkan_geo_widget;

class WidgetRouter extends \Drupal\json_form_widget\WidgetRouter {
  public function getWidgets()
  {
    return parent::getWidgets() + [
      'dkan_geo_widget' => 'handleGeoWidgetElement'
    ];
  }


  /**
   * Helper function for getting a geo widget textarea element.
   *
   * @param mixed $spec
   *   Object with spec for UI options.
   * @param array $element
   *   Element to convert into geo widget textarea.
   *
   * @return array
   *   The element configured as geo widget textarea.
   */
  public function handleGeoWidgetElement($spec, array $element) {
    $element = $this->handleTextareaElement($spec, $element);
    $element['#type'] = 'dkan_geo_widget';
    return $element;
  }

}
