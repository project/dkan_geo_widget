<?php

namespace Drupal\dkan_geo_widget\Element;

use Drupal\Core\Render\Element\Textarea;

/**
 * @FormElement("dkan_geo_widget")
 */
class DkanGeoWidget extends Textarea {

  public function getInfo()
  {
    $info = parent::getInfo();
    $info['#theme_wrappers'][] = 'geowidget_leaflet_wrapper';
    return $info;
  }

}
