## INTRODUCTION

The DKAN Geo Widget module allows to use a leaflet and leaflet geoman based
widget for the DKAN schema.

You can create markers and shapes on a leaflet map and save them as GeoJSON
in the related (via schema ui) field.

This brings in a feature again that DKAN D7 provided with [Leaflet widget module](https://www.drupal.org/project/leaflet_widget).

## REQUIREMENTS

- [DKANv2](https://github.com/GetDKAN/dkan) is required, especially the json_form_widget submodule

the *json_form_widget* module needs to be enabled beforehand, it will not get
automatically enabled due to https://github.com/GetDKAN/dkan/discussions/4149

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
- use *dkan_geo_widget* as widget key for the property in the .ui schema (dataset.ui.json for example)

## MAINTAINERS

Current maintainers:

- Stefan Korn (stefankorn) - https://www.drupal.org/u/stefankorn

