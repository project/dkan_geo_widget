(function($, Drupal, once, drupalSettings) {

  // Adopted from
  // https://git.drupalcode.org/project/leaflet_widget/-/blob/8.x-2.x/js/widget.js?ref_type=heads
  // https://git.drupalcode.org/project/leaflet/-/blob/10.2.x/js/leaflet.widget.js?ref_type=heads

  Drupal.behaviors.dkan_geo_widget = {
    attach: function (context, settings) {
      $('.geo-widget').each(function(index){
        let mapid = $(this).attr('id');
        $(once('dkan_geo_widget', 'textarea[name="'+$(this).data('geoinput')+'"]', context)).
        each(function () {
          new Drupal.dkan_geo_widget($(this).attr('name'), mapid);
        })
      });
    }
  };

  Drupal.dkan_geo_widget = function(name, mapid) {
    this.drawnItems = new L.FeatureGroup();
    this.textElement = $('textarea[name="'+name+'"]');
    this.textElement.on('change', this.textElement, $.proxy(this.update_map, this));
    this.map = L.map(mapid);
    this.update_map();
    this.set_leaflet_map();
  };

  Drupal.dkan_geo_widget.prototype.set_leaflet_map = function() {

    const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(this.map);

    this.map.addLayer(this.drawnItems);

    this.map.pm.addControls({
      position: 'topright'
    });

    this.map.on('pm:create', function(event){
      var layer = event.layer;
      this.drawnItems.addLayer(layer);
      this.update_text();

      // listen to changes on the new layer
      layer.on('pm:edit', function(event) {
        this.update_text();
      }, this);

      // listen to drags on the new layer
      layer.on('pm:dragend', function(event) {
        this.update_text();
      }, this);

    }, this);

    // listen to removal of layer.
    this.map.on('pm:remove', function(event) {
      this.drawnItems.removeLayer(event.layer);
      this.update_text();
    }, this);

    // listen to cuts on the new layer
    this.map.on('pm:cut', function (event) {
      // Cutting a layer return a new layer. The old layer has to be removed
      this.drawnItems.removeLayer(event.originalLayer);
      this.drawnItems.addLayer(event.layer);
      this.update_text();
    }, this);



  };

  Drupal.dkan_geo_widget.prototype.update_text = function () {
    this.textElement.val(JSON.stringify(this.drawnItems.toGeoJSON()));
  };

  Drupal.dkan_geo_widget.prototype.update_map = function () {
    var value = this.textElement.val();

    // Nothing to do if we don't have any data.
    if (value.length == 0) {
      this.drawnItems.eachLayer(function(layer) {
        this.map.removeLayer(layer);
      }, this);
      this.map.setView([50.179167, 9.458333], 13);
      return;
    }


    try {
      var obj = L.geoJSON(JSON.parse(value))
    } catch (error) {
      if (window.console) console.error(error.message);
      return;
    }

    this.drawnItems.eachLayer(function(layer) {
      this.map.removeLayer(layer);
    }, this);

    obj.eachLayer(function(layer) {

      // listen to changes on the new layer
      layer.on('pm:edit', function(event) {
        this.update_text();
      }, this);

      // listen to drags on the new layer
      layer.on('pm:dragend', function(event) {
        this.update_text();
      }, this);

      this.drawnItems.addLayer(layer);
      //layer.addTo(this.drawnItems);
    }, this);

    if (obj.getBounds !== undefined && typeof obj.getBounds === 'function') {
      // For objects that have defined bounds or a way to get them
      this.map.fitBounds(obj.getBounds());
    } else if (obj.getLatLng !== undefined && typeof obj.getLatLng === 'function') {
      this.map.panTo(obj.getLatLng());
    }




  };

}(jQuery, Drupal, once, drupalSettings));
